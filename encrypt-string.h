#pragma once

#include <string>

#define STRINGIFY_INTERNAL(x) #x
#define STRINGIFY(x) STRINGIFY_INTERNAL(x)
#define SEED __DATE__ ":" __TIME__ ":" __FILE__ ":" STRINGIFY(__LINE__) ":" STRINGIFY(__COUNTER__)
​
constexpr uint32_t kInitialHashSeed = 0x811c9dc5;
constexpr uint32_t kHashPrime = 0x1000193;
​
constexpr uint32_t fnv1a_hash_value(uint32_t current, uint32_t value)
{
    return static_cast<uint32_t>((static_cast<uint64_t>(current ^ value) * kHashPrime) & 0xffffffff);
}
​
template<typename CharType>
constexpr uint32_t fnv1a_32(CharType const* const str, size_t len) {
    uint32_t value = kInitialHashSeed;
    for (size_t i = 0; i < len; ++i) {
        value = fnv1a_hash_value(value, str[i]);
    }
    return value;
}
​
template<typename CharType, size_t N>
constexpr void encrypt(uint8_t dest[N], CharType const* src, uint32_t key)
{
    uint32_t originalKey = key;
    size_t dataIndex = 0;
​
    for (size_t i = 0; i < N / sizeof(CharType) - 1; ++i)
    {
        for (size_t j = 0; j < sizeof(CharType); ++j)
        {
            uint8_t const c = static_cast<uint8_t>((src[i] >> 8 * j) & 0xff);
            dest[dataIndex++] = static_cast<uint8_t>((c ^ key) & 0xff);
            key = fnv1a_hash_value(key, c);
        }
    }
​
    for (size_t j = 0; j < sizeof(CharType); ++j)
    {
        dest[dataIndex++] = static_cast<uint8_t>((originalKey >> 8 * j) ^ ~0u);
    }
}
​
template<typename StringType, size_t N>
StringType decrypt(uint8_t const* src)
{
    StringType result(N, 0);
    size_t srcIndex = 0;
​
    uint32_t key = 0;
    for (size_t j = 0; j < sizeof(StringType::value_type); ++j)
    {
        key |= src[N - sizeof(StringType::value_type) + j] << 8 * j;
    }
    key ^= ~0u;
​
    for (size_t i = 0; i < N / sizeof(StringType::value_type) - 1; ++i)
    {
        for (size_t j = 0; j < sizeof(StringType::value_type); ++j)
        {
            uint8_t const b = (src[srcIndex++] ^ key) & 0xff;
            result[i] |= b << 8 * j;
            key = fnv1a_hash_value(key, b);
        }
    }
​
    return result;
}
​
#define ENCRYPT_STRING_IMPL(str, seed, stringtype)                          \
([]() -> stringtype const& {                                                \
    struct EncryptedString {                                                \
        constexpr EncryptedString() {                                       \
            size_t const seedLength = sizeof(seed) / sizeof(seed[0]) - 1;   \
            uint32_t const key = fnv1a_32(seed, seedLength);                \
            encrypt<stringtype::value_type, sizeof(str)>(mData, str, key);  \
        }                                                                   \
                                                                            \
        stringtype get() const {                                            \
            return decrypt<stringtype, sizeof(str)>(mData);                 \
        }                                                                   \
                                                                            \
    private:                                                                \
        uint8_t mData[sizeof(str)]{};                                       \
    };                                                                      \
                                                                            \
    static constexpr EncryptedString estr;                                  \
    static const stringtype dstr = estr.get();                              \
    return dstr;                                                            \
})()
​
#define ENCRYPT_STRING(str) ENCRYPT_STRING_IMPL(str, SEED ":" str, std::basic_string<std::decay_t<decltype(str[0])>>)
