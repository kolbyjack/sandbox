#include <assert.h>
#include <stdio.h>

#include <mutex>
#include <shared_mutex>

template<typename ValueType, typename LockType>
class LockedValue final
{
public:
    LockedValue(ValueType& value, std::shared_mutex& mutex) : mValue(value), mLock(mutex) {}
    LockedValue(ValueType& value, std::shared_mutex& mutex, std::try_to_lock_t t) : mValue(value), mLock(mutex, t) {}
    LockedValue(LockedValue<ValueType, LockType> const&) = delete;
    LockedValue(LockedValue<ValueType, LockType>&& value) = default;

    LockedValue& operator=(LockedValue<ValueType, LockType> const&) = delete;
    LockedValue& operator=(LockedValue<ValueType, LockType>&&) = default;

    bool ownsLock() const { return mLock.owns_lock(); }

    ValueType& unwrap()
    {
        assert(ownsLock());
        return mValue;
    }

private:
    ValueType& mValue;
    LockType mLock;
};

template<typename ValueType>
class Mutex final
{
public:
    using ReadLockType = LockedValue<ValueType const, std::shared_lock<std::shared_mutex>>;
    using WriteLockType = LockedValue<ValueType, std::unique_lock<std::shared_mutex>>;

    Mutex() = default;
    Mutex(Mutex<ValueType> const&) = delete;
    Mutex(Mutex<ValueType>&&) = default;
    explicit Mutex(ValueType const& value) : mValue(value) {}
    explicit Mutex(ValueType&& value) : mValue(std::move(value)) {}

    Mutex& operator=(Mutex<ValueType> const&) = delete;
    Mutex& operator=(Mutex<ValueType>&&) = default;

    ReadLockType lock() { return ReadLockType(mValue, mLock); }
    ReadLockType tryLock() { return ReadLockType(mValue, mLock, std::try_to_lock_t()); }
    template<class Rep, class Period>
    ReadLockType tryLockFor(std::chrono::duration<Rep, Period> const& timeout) { return ReadLockType(mValue, mLock, timeout); }
    template<class Clock, class Duration>
    ReadLockType tryLockUntil(std::chrono::time_point<Clock, Duration> const& deadline) { return ReadLockType(mValue, mLock, deadline); }

    WriteLockType lockMutable() { return WriteLockType(mValue, mLock); }
    WriteLockType tryLockMutable() { return WriteLockType(mValue, mLock, std::try_to_lock_t()); }
    template<class Rep, class Period>
    WriteLockType tryLockMutableFor(std::chrono::duration<Rep, Period> const& timeout) { return WriteLockType(mValue, mLock, timeout); }
    template<class Clock, class Duration>
    WriteLockType tryLockMutableUntil(std::chrono::time_point<Clock, Duration> const& deadline) { return WriteLockType(mValue, mLock, deadline); }

private:
    ValueType mValue;
    std::shared_mutex mLock;
};

struct Foo
{
    int ival = 0;
    float fval = 0.0f;
};

int main()
{
    Mutex<Foo> m;

    {
        auto writer = m.lockMutable();
        ++writer.unwrap().ival;
        writer.unwrap().ival++;

        auto reader = m.tryLock();
        printf("assert fails!: ival=%d\n", reader.unwrap().ival);
    }

    printf("ival=%d\n", m.lock().unwrap().ival);

    return 0;
}
